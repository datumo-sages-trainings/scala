package pl.datumo.training.part1

import scala.collection.immutable.{List, Map}

object PartOne {

  //exercise 1
  //Write a method that converts all strings in a list to their upper case.
  def upperCase(list: List[String]): List[String] = ???

  //exercise 2
  // Write a method that keep only odd numbers and add 3 to this values
  def filterOdds(list: List[Int]): List[Int] = ???

  //exercise 3
  // Write a method that takes first letter of each string and concat it into one string
  def concatFirstLetters(list: List[String]): String = ???

  //exercise 4
  // Write a method that counts all the string which has "a" letter inside
  def countStringsWithA(list: List[String]): Long = ???

  //exercise 5
  // Write a method that removes duplicates and sum it into long
  def removeDuplicatesAndSum(list: List[Int]): Long = ???

  //exercise 6
  // Write a method that sorts list in reverse, e.g 1, 4, 3, 2 should output 4, 3, 2, 1
  def sortReverse(list: List[Int]): List[Int] = ???

  //exercise 7
  // Write method that takes names without surnames
  // Names and surnames are separated by space
  def namesWithoutSurnames(list: List[String]): List[String] = ???

  //exercise 8
  // Write a method that returns a comma separated string based on a given list of integers.
  // Each element should be preceded by the letter 'e' if the number is even, and preceded by the letter 'o'
  // if the number is odd. For example, if the input list is (3,44), the output should be 'o3,e44'.
  def getEvenOrOddString(list: List[Int]): List[String] = ???
}
