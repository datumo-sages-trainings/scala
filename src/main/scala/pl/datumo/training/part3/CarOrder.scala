package pl.datumo.training.part3

abstract class CarOrder(manufacturer: String, price: Int)

case class BmwCarOrder(manufacturer: String, price: Int) extends CarOrder(manufacturer, price)
case class MercedesCarOrder(manufacturer: String, price: Int) extends CarOrder(manufacturer, price)

// return bmw order if manufacturer is bmw, mercedes order if manufacturer is mercedes,
// throw Illegal argument exception otherwise
object CarOrder {
  def apply(manufacturer: String, price: Int): CarOrder = {
    ???
  }
}