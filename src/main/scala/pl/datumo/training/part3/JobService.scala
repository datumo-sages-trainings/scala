package pl.datumo.training.part3

case class JobStatus(jobId: Int, status: String)

object JobStatus {
  val `new` = "new"
  val running = "running"
  val finished = "finished"
}

// write method which returns new status with old id
object JobService {
  def changeJobStatus(jobStatus: JobStatus, newStatus: String): JobStatus = {
    ???
  }
}
