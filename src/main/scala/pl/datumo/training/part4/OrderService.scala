package pl.datumo.training.part4

case class Item(id: Int, price: Int)
case class Order(id: Int, parts: Seq[Item])

object OrderService {
  def meanSoldItemPrice(orders: Seq[Order]): Double = {
    ???
  }

  def medianSoldItemPrice(orders: Seq[Order]): Double = {
    ???
  }

  def differencesBetweenMedianAndRealValueOfSoldItemPrice(orders: Seq[Order]): Seq[Double] = {
    ???
  }
}
