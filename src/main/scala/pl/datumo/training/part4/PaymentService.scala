package pl.datumo.training.part4

case class Payment(id: Int, amount: Int, method: String)
case class DaySummary(date: String, payments: Seq[Payment])

object PaymentService {
  def transformToMap(payments: Seq[Payment]): Map[Int, Payment] = {
    ???
  }

  def blikPaymentWithoutAuthorization(payments: Seq[Payment]): Seq[Payment] = {
    ???
  }

  def blikPaymentWithoutAuthorizationSummary(payments: Seq[Payment]): Int = {
    ???
  }

  def allPaymentsFromSpecificDates(dates: Seq[String], weeklySummary: Map[String, Seq[Payment]]): Seq[Payment] = {
    ???
  }
}
