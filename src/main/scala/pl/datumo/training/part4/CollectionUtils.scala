package pl.datumo.training.part4

object CollectionUtils {

  // write method which returns mean value of all elements of the list
  def mean(seq: Seq[Int]): Double = {
    ???
  }

  // write method which returns median value of all elements of the list
  def median(seq: Seq[Int]): Int = {
    ???
  }
}
