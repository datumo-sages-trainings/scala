package pl.datumo.training.part2

import scala.collection.immutable.List

object PartTwo {

  //exercise 1
  //sort a given list
  def sort(list: List[Int]): List[Int] = ???

  //exercise 2
  //convert a list to a set
  def convertListToSet(list: List[Int]): Set[Int] = ???

  //exercise 3
  //test if a map contains a mapping for the specified key
  def mapContainsKey(map: Map[Int, Int], key: Int): Boolean = ???

  //exercise 4
  //test if a map contains a mapping for the specified value
  def mapContainsValue(map: Map[Int, Int], value: Int): Boolean = ???

  //exercise 5
  //reverse elements in a list
  def reverse(list: List[Int]): List[Int] = ???

  //exercise 6
  //Given an List of Strings, return a Map<String, Integer> containing a key for every different
  //string in the array, and the value is that string's length.
  def wordLen(list: List[String]): Map[String, Int] = ???

  //exercise 7
  // Write a method that finds first string longer than 5. If no element has been found
  // throw an exception of class IllegalArgumentException
  def findStringLongerThan5(list: List[String]): String = ???
}
