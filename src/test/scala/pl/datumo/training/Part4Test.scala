package pl.datumo.training

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import pl.datumo.training.part4._

class Part4Test extends AnyFlatSpec with Matchers {

  val payment1: Payment = Payment(1, 23, "blik")
  val payment2: Payment = Payment(2, 43, "online")
  val payment3: Payment = Payment(3, 433, "cash")
  val payment4: Payment = Payment(4, 13, "blik")
  val payment5: Payment = Payment(5, 433, "online")
  val payment6: Payment = Payment(6, 433, "cash")
  val payment7: Payment = Payment(7, 223, "blik")
  val payment8: Payment = Payment(8, 423, "online")
  val payment9: Payment = Payment(9, 433, "cash")

  "DuplicateRemover" should "removes duplicates from collections" in {
    val seq1 = Seq(1, 2, 3, 1, 2, 3, 3, 2, 1, 4)
    val seq2 = Seq(9, 2, 3, 9, 4, 3, 4)

    DuplicatesRemover.removeDuplicates(seq1) shouldBe Set(1, 2, 3, 4)
    DuplicatesRemover.removeDuplicates(seq2) shouldBe Set(2, 3, 4, 9)
  }

  "CollectionUtils mean method" should "calculate mean" in {
    val col: Seq[Int] = Seq(1, 2, 4, 6, 12, 14)

    CollectionUtils.mean(col) shouldBe 6.5
    the [IllegalArgumentException] thrownBy CollectionUtils.mean(Seq.empty) should have message "mean cannot be calculated for empty collection"
  }

  "CollectionUtils median method" should "calculate median" in {
    val col: Seq[Int] = Seq(1, 2, 4, 6, 12, 14)
    val col2: Seq[Int] = Seq(1, 2, 4, 6, 7, 12, 14)

    CollectionUtils.median(col) shouldBe 5
    CollectionUtils.median(col2) shouldBe 6
    the [IllegalArgumentException] thrownBy CollectionUtils.median(Seq.empty) should have message "median cannot be calculated for empty collection"
  }

  "PaymentService transformToMap method" should "transform seq of payments into Map indexed by id" in {
    val payments = Seq(payment1, payment2, payment3)

    PaymentService.transformToMap(payments) shouldBe Map(
      payment1.id -> payment1,
      payment2.id -> payment2,
      payment3.id -> payment3
    )
  }

  "PaymentService blikPaymentWithoutAuthorization method" should
    "return seq of payments which method was equal to Blik and amount was lower than 50" in {
    val payments: Seq[Payment] = Seq(payment1, payment2, payment3, payment4, payment5, payment6, payment7, payment8, payment9)

    PaymentService.blikPaymentWithoutAuthorization(payments) shouldBe Seq(payment1, payment4)
  }

  "PaymentService blikPaymentWithoutAuthorizationSummary method" should
    "return total sum of blik payment without authorization (amount lower than 50)" in {
    val payments: Seq[Payment] = Seq(payment1, payment2, payment3, payment4, payment5, payment6, payment7, payment8, payment9)

    PaymentService.blikPaymentWithoutAuthorizationSummary(payments) shouldBe 36
  }

  "PaymentService getAllPaymentsFromSpecificDates method" should
    "return all payments occurred in specific dates" in {
    val weeklyPaymentSummary: Map[String, Seq[Payment]] = Map(
      "2019-12-09" -> Seq(payment1, payment2),
      "2019-12-10" -> Seq(payment3),
      "2019-12-11" -> Seq(payment4, payment5),
      "2019-12-12" -> Seq(payment6),
      "2019-12-13" -> Seq(payment7, payment8),
      "2019-12-14" -> Seq(payment9)
    )

    val daysOfSuspectTraffic = Seq("2019-12-11", "2019-12-13")

    PaymentService.allPaymentsFromSpecificDates(daysOfSuspectTraffic, weeklyPaymentSummary) shouldBe
      Seq(payment4, payment5, payment7, payment8)
  }

  "OrderService meanSoldItemPrice" should "calculate mean sold item price" in {
    val items: Seq[Item] = Seq(Item(1, 10), Item(2, 15), Item(3, 5), Item(4, 55))
    val orders: Seq[Order] = Seq(
      Order(1, Seq(items.head, items(3))),
      Order(2, Seq(items(1), items(3)))
    )

    OrderService.meanSoldItemPrice(orders) shouldBe 33.75
  }

  "OrderService medianSoldItemPrice" should "calculate median of sold item prices" in {
    val items: Seq[Item] = Seq(Item(1, 10), Item(2, 15), Item(3, 5), Item(4, 55))
    val orders1: Seq[Order] = Seq(
      Order(1, Seq(items.head, items(3))),
      Order(2, Seq(items(1), items(3)))
    )
    val orders2: Seq[Order] = Seq(
      Order(1, Seq(items.head, items(3))),
      Order(2, Seq(items(1), items(3))),
      Order(2, Seq(items(1)))
    )

    OrderService.medianSoldItemPrice(orders1) shouldBe 35
    OrderService.medianSoldItemPrice(orders2) shouldBe 15
  }

  "OrderService differencesBetweenMedianAndRealValueOfSoldItemPrice" should
    "calculate differences between median and values of sold item price Returned Seq should be sorted" in {
    val items: Seq[Item] = Seq(Item(1, 10), Item(2, 15), Item(3, 5), Item(4, 55))
    val orders1: Seq[Order] = Seq(
      Order(1, Seq(items.head, items(3))),
      Order(2, Seq(items(1), items(3)))
    )
    val orders2: Seq[Order] = Seq(
      Order(1, Seq(items.head, items(3))),
      Order(2, Seq(items(1), items(3))),
      Order(2, Seq(items(1)))
    )

    OrderService.differencesBetweenMedianAndRealValueOfSoldItemPrice(orders1) shouldBe Seq(-25,-20,20,20)
    OrderService.differencesBetweenMedianAndRealValueOfSoldItemPrice(orders2) shouldBe Seq(-5,0,0,40,40)
  }
}
