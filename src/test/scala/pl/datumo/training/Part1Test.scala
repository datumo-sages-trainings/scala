package pl.datumo.training

import org.scalatest.flatspec.AnyFlatSpec
import pl.datumo.training.part1.PartOne

class Part1Test extends AnyFlatSpec {

  "PartOne" should "converts all strings to their upper case" in {
    assertResult(List("AAA", "BB", "C"))(PartOne.upperCase(List("aaa", "bb", "c")))
    assertResult(List("QWERTY"))(PartOne.upperCase(List("qWeRtY")))
  }

  it should "keep only odd numbers and add 3 to this values" in {
    assertResult(List(8, 6, 20))(PartOne.filterOdds(List(5, 44, 12, 3, 17, 6)))
    assertResult(List(92))(PartOne.filterOdds(List(89, 6, 14)))
  }

  it should "concat first letters of all elements in the list" in {
    assertResult("abc")(PartOne.concatFirstLetters(List("anna", "barbara", "czeslaw")))
    assertResult("AwD")(PartOne.concatFirstLetters(List("Adam", "wieslaw", "Daniel")))
    assertResult(" ")(PartOne.concatFirstLetters(List(" ")))
  }

  it should "counts all the string which has \"a\" letter inside" in {
    assertResult(3)(PartOne.countStringsWithA(List("Ala", "ma", "kota")))
    assertResult(1)(PartOne.countStringsWithA(List("aaa", "bb", "c", "dddd")))
  }

  it should "removes duplicates and sum it into long" in {
    assertResult(283)(PartOne.removeDuplicatesAndSum(List(234,23,6,1,4,12,6,3,3,23)))
    assertResult(10)(PartOne.removeDuplicatesAndSum(List(1,1,2,2,2,3,4,4)))
  }

  it should "sorts list in reverse" in {
    assertResult(List(4, 3, 2, 1))(PartOne.sortReverse(List(1, 4, 3, 2)))
    assertResult(List(44, 17, 12, 5, 3))(PartOne.sortReverse(List(5, 44, 12, 3, 17)))
  }

  it should "takes names without surnames" in {
    assertResult(List("Donald", "John", "William"))(PartOne.namesWithoutSurnames(List("Donald Trump", "John Rambo", "William Shakespeare")))
    assertResult(List("Britney", "Leonardo"))(PartOne.namesWithoutSurnames(List("Britney Spears", "Leonardo DiCaprio")))
  }

  it should "get even or odd string" in {
    assertResult(List("o5", "e44", "e12", "o3"))(PartOne.getEvenOrOddString(List(5, 44, 12, 3)))
    assertResult(List("o89", "e6", "e14"))(PartOne.getEvenOrOddString(List(89, 6, 14)))
  }
}
