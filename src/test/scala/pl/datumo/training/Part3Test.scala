package pl.datumo.training

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import pl.datumo.training.part3._

class Part3Test extends AnyFlatSpec with Matchers {

  "Point toString method" should "return string in defined format" in {
    val point1 = new Point(1, 5)
    val point2 = new Point(5, 1)

    point1.toString shouldBe "AssaAbloyPoint(1, 5)"
    point2.toString shouldBe "AssaAbloyPoint(5, 1)"
  }

  "Point equals method" should "return true when objects have equals fields otherwise e" in {
    val point1 = new Point(1, 5)
    val point2 = new Point(1, 5)
    val point3 = new Point(5, 1)

    assert(point1 == point2)
    assert(point1 != point3)
    assert(point2 != point3)
  }

  "JobService" should "correctly change job status" in {
    val newJob = JobStatus(1, JobStatus.`new`)

    JobService.changeJobStatus(newJob, JobStatus.running) shouldBe JobStatus(1, JobStatus.running)
    JobService.changeJobStatus(newJob, JobStatus.finished) shouldBe JobStatus(1, JobStatus.finished)
  }

  "CarOrder apply method" should "correctly create order based on manufacturer" in {
    CarOrder.apply("bmw", 100) shouldBe a[BmwCarOrder]
    CarOrder.apply("mercedes", 100) shouldBe a[MercedesCarOrder]
    the [IllegalArgumentException] thrownBy CarOrder.apply("audi", 100) should have message "our company does not sell audi cars"
    the [IllegalArgumentException] thrownBy CarOrder.apply("opel", 100) should have message "our company does not sell opel cars"
  }
}
