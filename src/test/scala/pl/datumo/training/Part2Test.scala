package pl.datumo.training

import org.scalatest.flatspec.AnyFlatSpec
import pl.datumo.training.part2.PartTwo

class Part2Test extends AnyFlatSpec {

  "PartOne" should "sort a given list" in {
    assertResult(List(2, 3, 4))(PartTwo.sort(List(4, 2, 3)))
    assertResult(List(9, 14, 28, 37))(PartTwo.sort(List(37, 9, 14, 28)))
  }

  it should "convert list to set" in {
    val list = List(4, 3, 3)
    val set = Set(4, 3)
    assertResult(true)(PartTwo.convertListToSet(list).size.equals(list.size - 1))
    assertResult(true)(PartTwo.convertListToSet(list).isInstanceOf[Set[Int]])
  }

  it should "test if a map contains a mapping for the specified key" in {
    val map = Map(32 -> 2, 654 -> 1, 546 -> 5, 12 -> 4)
    assertResult(true)(PartTwo.mapContainsKey(map, 654))
    assertResult(true)(PartTwo.mapContainsKey(map, 12))
    assertResult(false)(PartTwo.mapContainsKey(map, 123))
    assertResult(false)(PartTwo.mapContainsKey(map, 99))
  }

  it should "test if a map contains a mapping for the specified value" in {
    val map = Map(32 -> 2, 654 -> 1, 546 -> 5, 12 -> 4)
    assertResult(true)(PartTwo.mapContainsValue(map, 2))
    assertResult(true)(PartTwo.mapContainsValue(map, 5))
    assertResult(false)(PartTwo.mapContainsValue(map, 123))
    assertResult(false)(PartTwo.mapContainsValue(map, 99))
  }

  it should "reverse elements in a list" in {
    assertResult(List(3, 2, 4))(PartTwo.reverse(List(4, 2, 3)))
    assertResult(List(28, 14, 9, 37))(PartTwo.reverse(List(37, 9, 14, 28)))
  }

  it should "get string's length map" in {
    assertResult(Map("salt" -> 4, "sugar" -> 5))(PartTwo.wordLen(List("salt", "sugar")))
    assertResult(Map("tea" -> 3, "coffee" -> 6, "water" -> 5))(PartTwo.wordLen(List("tea", "coffee", "water")))
  }

  it should "find first string longer than 5" in {
    assertResult("Krakow")(PartTwo.findStringLongerThan5(List("", "Lodz", "Abcde", "Krakow", "Zabrze")))
    assertThrows[IllegalArgumentException](PartTwo.findStringLongerThan5(List("", "Lodz", "abcde")))
    assertThrows[IllegalArgumentException](PartTwo.findStringLongerThan5(List()))
  }
}
